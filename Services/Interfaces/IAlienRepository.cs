using System.Collections.Generic;
using MoonHumans.Models;
using System.Threading.Tasks;

namespace MoonHumans.Services.Interfaces{
    public interface IAlienService
    {
           Task<IEnumerable<Alien>> GetAllAliens();
            Task<Alien> GetAlienById(int id);
    }
}