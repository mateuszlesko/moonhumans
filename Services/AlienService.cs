using System.Collections.Generic;
using System.Threading.Tasks;
using MoonHumans.Models;
using MoonHumans.Data;
using MoonHumans.Repositories.Interfaces;
using MoonHumans.Repositories;

namespace MoonHumans.Services{
    public class AlienService :
    Interfaces.IAlienService{
     
        private readonly AlienRepository alienRepository;
        public AlienService(MoonHumansDbContext context){
         alienRepository= new AlienRepository(context);
        }

        public async Task<Alien> GetAlienById(int id) => await  alienRepository.GetModelDataById(id);

        public async Task<IEnumerable<Alien>> GetAllAliens() => await  alienRepository.GetAllModelData();
        // public void ChangeAlienOwner(int alienCoinId, int bagId){
        //     Alien alien = Task.Run( () => GetAlienById(alienId)).Result; //znalezc inny sposob aby uzyc async w sync metodzie
        //     AlienRepository.UpdateDataModel(alien);
        // }
    }
}
    