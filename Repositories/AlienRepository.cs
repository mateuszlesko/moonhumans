using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using MoonHumans.Repositories.Interfaces;
using MoonHumans.Data;
using MoonHumans.Models;

namespace MoonHumans.Repositories{
    public class AlienRepository : IDisposable, IAlienRepository, IRepository<Alien>{

        readonly MoonHumansDbContext _context;

        public AlienRepository(MoonHumansDbContext context){
            _context = context;
        }
        public void print(){
            Console.WriteLine("Print the text");
        }
        public async Task<ICollection<Alien>> GetAllModelData() => await _context.Aliens.ToListAsync();

        public async Task<Alien> GetModelDataById(int id) =>  await _context.Aliens.FirstOrDefaultAsync(alien => alien.ID == id);
        
        public void CreateDataModel(Alien alien){
             _context.Aliens.Add(alien);
        }

        public void UpdateDataModel(Alien alien){
            _context.Update(alien);
            SaveSync();
        }
        public void DeleteDataModel(Alien alien){
            _context.Aliens.Remove(alien);
            SaveSync();
        }
        private void SaveSync() => _context.SaveChanges();

        private async Task SaveAsync() => await _context.SaveChangesAsync();
        private bool disposed = false;
        
        protected virtual void Dispose(bool disposing){
            if(!this.disposed)
                if(disposing)
                    _context.Dispose();
                
            this.disposed = true;
        }

        public void Dispose(){
            Dispose(true);
            GC.SuppressFinalize(this); // GarbageCollector zwalnia zasoby
        }
    }
}