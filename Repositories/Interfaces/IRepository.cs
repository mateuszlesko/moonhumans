using System.Collections.Generic;
using System.Threading.Tasks;

namespace MoonHumans.Repositories.Interfaces{
    public interface IRepository<Model>{
        Task<ICollection<Model>> GetAllModelData();
        void CreateDataModel(Model model);
        void UpdateDataModel(Model model);
        void DeleteDataModel(Model model);

    }
}